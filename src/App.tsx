import { FC } from "react";
import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom";
import { HomePage } from "./components/homePage";
import { LoginPage } from "./components/loginPage";
import { ProfilePage } from "./components/profilePage";
import { UserProvider } from "./provider/user";

export const App: FC = () => {
  return (
    <UserProvider>
      <Router>
        <div className="App">
          <header
            className="App-header"
            style={{ display: "flex", justifyContent: "space-between" }}
          >
            <Link to="/login">Login</Link>
            <Link to="/">Home</Link>
            <Link to="/profile">Profile</Link>
          </header>
        </div>
        <Switch>
          <Route path="/login">
            <LoginPage />
          </Route>
          <Route path="/profile">
            <ProfilePage />
          </Route>
          <Route path="/">
            <HomePage />
          </Route>
        </Switch>
      </Router>
    </UserProvider>
  );
};
