import { FC, createContext, useState, Dispatch, SetStateAction } from "react";
import { QueryClient, QueryClientProvider } from "react-query";

export type User = {
  id: number;
  name: string;
};

export const UserContext = createContext<User | undefined>(undefined);
export const UserSetContext = createContext<
  Dispatch<SetStateAction<User | undefined>>
>(() => {});

export const UserProvider: FC = ({ children }) => {
  const [user, setUser] = useState<User | undefined>(undefined);
  const queryClient = new QueryClient();

  return (
    <QueryClientProvider client={queryClient}>
      <UserSetContext.Provider value={setUser}>
        <UserContext.Provider value={user}>{children}</UserContext.Provider>
      </UserSetContext.Provider>
    </QueryClientProvider>
  );
};
