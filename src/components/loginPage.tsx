import { FC, useState } from "react";
import { useSearchUser } from "../hooks/useSearchUser";

export const LoginPage: FC = () => {
  console.log("LoginPage rendered");

  const [username, setUsername] = useState("");
  const {setSearchTerm, ...query} = useSearchUser();

  if (query.isError) {
      console.log('error encountered in query');
      return <p>{`error type: ${query.error}`}</p>
  }

  return (
    <div>
      <h1 style={{ textAlign: "center" }}>Login Page</h1>
      <form
        onSubmit={(e) => {
          setSearchTerm(username);
          e.preventDefault();
        }}
      >
        <label>username</label>
        <br />
        <input
          type="text"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
        />
        <br />
        <button type="submit">Submit</button>
      </form>
    </div>
  );
};
