import { FC, useContext } from "react";
import { UserContext } from "../provider/user";

export const HomePage: FC = () => {
  console.log("HomePage rendered");
  const user = useContext(UserContext);

  return (
    <div>
      <h1 style={{ textAlign: "center" }}>Home Page</h1>
      <p>Current Username: '{user?.name}'</p>
    </div>
  );
};
