import { useContext } from "react";
import { useQuery } from "react-query";
import { User, UserSetContext } from "../provider/user";
import axios from "axios";

export const useUser = (searchTerm: string) => {
  const setUser = useContext(UserSetContext);
  const { data: user, ...query } = useQuery(
    "user",
    async () => {
      const resp = await axios.get<User>(
        `http://localhost:3001/users?name=${searchTerm}`
      );
      return resp.data;
    }
  );

  if (query.isError) {
    console.log("user GET error: ", query.error);
  }

  setUser(user);
};
