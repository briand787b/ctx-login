import axios from "axios";
import { useContext, useEffect, useState } from "react";
import { useQuery } from "react-query";
import { User, UserSetContext } from "../provider/user";

export const useSearchUser = () => {
  console.log("executing useSearchUser");
  const [searchTerm, setSearchTerm] = useState("");

  console.log("searchTerm: ", searchTerm);
  const { data: users, ...query } = useQuery(["user", searchTerm], async () => {
    const resp = await axios.get<User[]>(
      `http://localhost:3001/users?name=${searchTerm}`
    );
    console.log("response data: ", resp);
    return resp.data;
  });

  const user = users && users[0]

  const setUser = useContext(UserSetContext);
  useEffect(() => {
    setUser(user);
  });

  return { setSearchTerm, ...query };
};
